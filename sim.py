from random import shuffle

FALL = 1#0
SPRING = 0#1

class Course:
	def __init__(self, name, hrs = 3, prereqs = [], coreqs = [], season = -1, ignoreFirstSem = False):
		self.name = name
		self.hrs = hrs
		self.prereqs = prereqs
		self.coreqs = coreqs
		self.season = season
		self.ignoreFirstSem = ignoreFirstSem
	def canTake(self, coursesTaken, coursesTaking, semsTaken):
		for prereq in self.prereqs:
			if prereq not in coursesTaken:
				return False
		for coreq in self.coreqs:
			if coreq not in coursesTaking:
				return False
		if self.season != -1 and semsTaken % 2 != self.season:
			return False
		if semsTaken == 0 and self.ignoreFirstSem:
			return False
		return True
	def getImportance(self, otherCourses): # number of layers of courses relying on this one
		returnVal = 0
		for course in otherCourses:
			if self in course.prereqs:
				returnVal = max(returnVal, course.getImportance(otherCourses) + 1)
		return returnVal
	def getAllCoreqs(self, fullList, alreadyFound = None):
		if alreadyFound == None:
			alreadyFound = {}
		if self in alreadyFound:
			raise Exception("should never happen")
		returnVal = [self]
		alreadyFound[self] = True
		for coreq in self.coreqs:
			if not coreq in alreadyFound:
				returnVal += coreq.getAllCoreqs(fullList, alreadyFound)
		for course in fullList:
			if not course in alreadyFound and self in course.coreqs:
				returnVal += course.getAllCoreqs(fullList, alreadyFound)
		return returnVal
	def __str__(self):
		return self.name

def makeSemesterCourseList(fullList):
	importances = {}
	for course in fullList:
		importances[course] = course.getImportance(fullList)
	sortedList = sorted(fullList, key = lambda x: importances[x], reverse = True)
	returnVal = []
	allCoursesTaken = []
	while len(sortedList) > 0:
		currentSem = []
		semHrs = 0
		for course in sortedList:
			if course in sortedList:
				allCoreqs = course.getAllCoreqs(fullList)
				hrsForCourse = 0
				for coreq in allCoreqs:
					hrsForCourse += coreq.hrs
				if hrsForCourse + semHrs <= 18:
					canTake = True
					for coreq in allCoreqs:
						canTake = canTake and coreq.canTake(allCoursesTaken, currentSem + allCoreqs, len(returnVal))
					if canTake:
						currentSem += allCoreqs
						sortedList = [x for x in sortedList if x not in allCoreqs]
						semHrs += hrsForCourse
		returnVal.append(currentSem)
		allCoursesTaken += currentSem
	return returnVal

def makeSemesterCourseListMinimizing(fullList, f, iters = 100):
	smallestAt = [makeSemesterCourseList(fullList)]
	smallest = f(smallestAt[0])
	for i in range(iters):
		shuffle(fullList)
		val = makeSemesterCourseList(fullList)
		fVal = f(val)
		if fVal < smallest:
			smallest = fVal
			smallestAt = [val]
		elif fVal == smallest:
			smallestAt += [val]
	return smallest, smallestAt

def courseListToStr(l):
	if not l: return str(l)
	returnVal = "Total semesters: " + str(len(l)) + "\n"
	for i in range(len(l)):
		sem = l[i]
		hrsInSem = 0
		for course in sem:
			hrsInSem += course.hrs
		returnVal += "S" + str(i+1) + ": " + str(hrsInSem) + "H: " + str(list(map(lambda x: str(x), sem))) + "\n"
	return returnVal

def courseNamed(l, n):
	for course in l:
		if course.name == n:
			return course
	raise Exception("couldnt find course "+n)

# TODO eece 2116 is a coreq for 2231, but it's not open to people who got credit for eece 2123
# ALSO TODO, fall and spring only mess things up

# MY COURSES, AS AN EXAMPLE
# cs = [Course("1151"), Course("2231", ignoreFirstSem = True), Course("3270", ignoreFirstSem = True), Course("math 2820"), Course("3250", ignoreFirstSem = True), Course("4959", 1, season = FALL), Course("3281")]
# cs += [Course("math 3320", season = FALL), Course("4260", season=FALL, prereqs=[courseNamed(cs, "3250"), courseNamed(cs, "math 2820")]), Course("4287", prereqs=[courseNamed(cs, "3281")]), Course("3258", season=SPRING)] # cs depth
# cs += [Course("4269", season=SPRING, prereqs=[courseNamed(cs, "4260")])]

cs = [Course("1151"), Course("2231"), Course("3270"), Course("3250"), Course("4959", 1, season = FALL), Course("4287"), Course("3258", season=SPRING)]
cs += [Course("4260", season=FALL, prereqs=[courseNamed(cs, "3250")])]
cs += [Course("4269", season=SPRING, prereqs=[courseNamed(cs, "4260")])]

# eece = [Course("eece 2112"), Course("eece 2218+L", 4, season=SPRING)]
# eece += [Course("eece 3214", prereqs=[courseNamed(eece, "eece 2112")])]
eece = [Course("eece 2218+L", 4, season=SPRING), Course("eece 3214")]

csmaster = [Course("cs master", prereqs=cs) for i in range(10)] # prereqs cs?

fullList = cs+eece #+csmaster
criteria = lambda x: len(x) * 18 + sum(map(lambda x: x.hrs, x[-1]))
smallest, smallestAt = makeSemesterCourseListMinimizing(fullList, criteria)
print("first in list:")
print(courseListToStr(smallestAt[0]))

mostCommon = {}
for i in smallestAt:
	for c in i[0]:
		if not c.name in mostCommon: mostCommon[c.name] = 0
		mostCommon[c.name] += 1

print("frequencies in first semester:")
print(mostCommon)
print()

sorted = list(mostCommon.keys())
sorted.sort(reverse = True, key = lambda x: mostCommon[x])
print("most common first:")
print(sorted)
print()

rightSchedule = None
for i in smallestAt:
	isRight = True
	for c in i[0]:
		if c.name not in sorted[0:len(i[0])]:
			isRight = False
	if isRight:
		rightSchedule = i
		break
print("schedule with the top n courses first semester:")
print(courseListToStr(rightSchedule))
